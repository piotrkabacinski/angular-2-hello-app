import { Component, EventEmitter } from "angular2/core";
import { basePath } from "../../common/index";

@Component({
    selector: "childComponent",
    templateUrl: basePath + "/dataflow/childComponent/child.template.html",
    inputs: ['messageFromParent'],
    outputs: ['messageToParent']
})

export class ChildComponent {

    private message: string;
    private messageFromParent: string | boolean = false;
    private messageToParent = new EventEmitter<any>();

    sendMessage() {
      this.messageToParent.emit( this.message );
    }

}
