import { Component, EventEmitter } from "angular2/core";
import { basePath, MenuComponent, HeaderComponent, SubpageComponent } from "../common/index";
import { ChildComponent } from "./childComponent/child.component";

@Component({
    templateUrl: basePath + "/dataflow/df.template.html",
    directives: [MenuComponent, HeaderComponent, SubpageComponent, ChildComponent],
})

export class DataFlowComponent {

    private message: string;
    private messageFromChild: string | boolean = false;
    private messageFromParent: string | boolean = false;
    private title: string = "Przepływ danych";

    sendMessage() {
        this.messageFromParent = this.message;
    }

    recieve(data: string) {
        this.messageFromChild = data;
    }

}
