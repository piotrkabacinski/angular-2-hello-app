import { Component } from "angular2/core";
import { Control, ControlGroup, FormBuilder, Validators, NgForm } from "angular2/common";
import { basePath, MenuComponent, HeaderComponent, SubpageComponent } from "../common/index";
import { fooValidator } from "../form/fooValidator";

@Component({
    templateUrl: basePath + "/form/form.template.html",
    directives: [MenuComponent, HeaderComponent, SubpageComponent]
})

export class FormComponent {

    private sampleForm: any;
    private formResults: any = {
        show: false
    };
    private title: string = "Formularz";

    constructor(
        private formBuilder: FormBuilder
    ) {

        this.sampleForm = this.formBuilder.group({
            "input": ["", Validators.compose([fooValidator.checkFoo])],
            "textarea": [ "Default value", Validators.compose([Validators.required])],
            "checkbox": [],
            "select": []
        });

    }

    displayFormResults() {

        let resultsHolder = this.sampleForm.controls;

        this.formResults = {
            input: resultsHolder.input.value,
            textarea: resultsHolder.textarea.value,
            checkbox: resultsHolder.checkbox.value,
            select: resultsHolder.select.value,
            show: true
        };

    }

}
