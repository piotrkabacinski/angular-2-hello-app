import { Control } from 'angular2/common'

export class fooValidator {

  static checkFoo( control: Control ) {

    if( control.value === "Foo" ) {

      return null;

    }

    return {
        "notFoo": true
    }

  }

}
