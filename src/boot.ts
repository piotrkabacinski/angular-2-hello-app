import { bootstrap } from "angular2/platform/browser";
import { PLATFORM_DIRECTIVES, provide } from "angular2/core";
import { HTTP_PROVIDERS } from "angular2/http";
import { ROUTER_PROVIDERS, Location, LocationStrategy, HashLocationStrategy } from "angular2/router";
import { AppComponent } from "./app/app.component";

bootstrap(AppComponent, [
    HTTP_PROVIDERS,
    ROUTER_PROVIDERS,
    provide(LocationStrategy, { useClass: HashLocationStrategy })
]);
