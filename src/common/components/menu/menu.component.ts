import { Component } from "angular2/core";
import { ROUTER_DIRECTIVES, Router, RouteParams } from "angular2/router";
import { basePath } from "../../index";

@Component({
    selector: "menu",
    templateUrl: basePath + "/common/components/menu/menu.template.html",
    directives: [ROUTER_DIRECTIVES],
    styles: [
        `nav {
      font-size: 0.75rem;
    }`]
})

export class MenuComponent { }
