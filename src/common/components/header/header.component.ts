import { Component } from "angular2/core";
import { basePath } from "../../index";

@Component({
    selector: "header",
    templateUrl: basePath + "/common/components/header/header.template.html",
})

export class HeaderComponent {}
