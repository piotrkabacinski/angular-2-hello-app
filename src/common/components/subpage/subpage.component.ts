import { Component } from "angular2/core";
import { basePath } from "../../index";

@Component({
    selector: "subpage",
    templateUrl: basePath + "/common/components/subpage/subpage.template.html",
    inputs: ["title"],
    styles: [`.subpage {
    color: #c30e2e;
    adding-top: 10px;
  }`]
})

export class SubpageComponent {

    public title: string = "Nie wiadomo";

}
