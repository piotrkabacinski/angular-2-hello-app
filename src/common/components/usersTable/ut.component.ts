import { Component } from "angular2/core";
import { basePath } from "../../index";

@Component({
    selector: "usersTable",
    templateUrl: basePath + "/common/components/usersTable/ut.template.html",
    inputs: ["users"]
})

export class UsersTableComponent {

  private users: Array<any> = [];

}
