import { Pipe, PipeTransform } from 'angular2/core';

@Pipe({
    name: 'bytesConverter',
    /**
     * "Angular executes an impure pipe during every component change detection cycle"
     * https://angular.io/docs/ts/latest/guide/pipes.html
     */
    pure: false
})

export class BytesConverter implements PipeTransform {

    transform( value, args ) {

       let calucalted = 0;

        switch( args[0] ) {

          case "kb":
            calucalted = value / 1024;
          break;

          case "mb":
            calucalted = value / 1048576;
          break;

          case "gb":
            calucalted = value / 1073741824;
          break;

          case "tb":
            calucalted = value / 1099511627776;
          break;

          default:

            calucalted = value;

        }

        return Math.round( calucalted * 100 ) / 100;

    }

}
