import { Injectable } from "angular2/core";

@Injectable()

export class StashService {

    static stash: any = {};

    static create(bucket: string) {

        this.stash[bucket] = {
            content: undefined
        };

        console.log( "Content stashed!" , this.stash );

    }

    static add(bucket: string = "", content: any) {

        if (this.stash[bucket]) {

            this.stash[bucket]["content"] = content;

            return this.stash;

        }

        console.error("No bucket \"" + bucket + "\" in stash!");

        return false;

    }

    static get(bucket: string = "") {

        if (this.stash[bucket]) {
            return this.stash[bucket].content;
        }

        console.error("No bucket \"" + bucket + "\" in stash!");

        return false;

    }

}
