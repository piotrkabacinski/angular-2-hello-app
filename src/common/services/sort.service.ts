import { Injectable } from "angular2/core";

@Injectable()

export class SortService {

    constructor() { }

    public sort(data: any = [], property: string = "", order: string = "desc") {

        let sorted = [];

        switch (order) {

            case "desc":

                sorted = data.sort(function(a, b) {
                    return a[property].toUpperCase() < b[property].toUpperCase();
                });

            break;

            default:

              sorted = data.sort(function(a, b) {
                  return a[property].toUpperCase() > b[property].toUpperCase();
              });

        }

        return sorted;

    }

}
