import { Component } from "angular2/core";
import { RouteParams, Router } from "angular2/router";
import { basePath, MenuComponent, HeaderComponent, SubpageComponent } from "../common/index";

@Component({
    templateUrl: basePath + "/params/params.template.html",
    directives: [MenuComponent, HeaderComponent, SubpageComponent]
})

export class ParamsComponent {

    private id: string;
    private foo: string;
    private title: string = "Test parametrów";

    constructor(
        private params: RouteParams,
        private router: Router
    ) {
        this.id = params.get("id");
        this.foo = params.get("foo");
    }

    goToForm() {

        this.router.parent.navigateByUrl("/form");

    }

}
