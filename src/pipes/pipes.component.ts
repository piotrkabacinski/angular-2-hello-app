import { Component } from "angular2/core";
import { basePath, MenuComponent, HeaderComponent, SubpageComponent, BytesConverter } from "../common/index";

@Component({
    templateUrl: basePath + "/pipes/pipes.template.html",
    directives: [MenuComponent, HeaderComponent, SubpageComponent],
    pipes: [BytesConverter]
})

export class PipesComponent {

    private value: number | boolean = false;
    private title: string = "Pipe";

}
