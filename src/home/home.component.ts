import { Component } from "angular2/core";
import { basePath, MenuComponent, HeaderComponent, SubpageComponent } from "../common/index";

@Component({
    templateUrl: basePath + "/home/home.template.html",
    directives: [MenuComponent, HeaderComponent, SubpageComponent]
})

export class HomeComponent {

    private name: string;
    private title: string = "Strona główna";
    
}
