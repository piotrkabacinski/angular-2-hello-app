import { Component } from "angular2/core";
import { ROUTER_DIRECTIVES, RouteConfig, Route, Redirect } from "angular2/router";
import { HomeComponent } from "../home/home.component";
import { ParamsComponent } from "../params/params.component";
import { FormComponent } from "../form/form.component";
import { DirectiveComponent } from "../directive/directive.component";
import { HttpComponent } from "../http/http.component";
import { ServicesComponent } from "../services/services.component";
import { DataFlowComponent } from "../dataflow/df.component";
import { PipesComponent } from "../pipes/pipes.component";
import "rxjs/Rx";

@Component({
    selector: "hello",
    template: "<router-outlet></router-outlet>",
    directives: [ROUTER_DIRECTIVES]
})

@RouteConfig([
    { path: "/", name: "Home", component: HomeComponent, useAsDefault: true },
    { path: "/params/:id/:foo", name: "Params", component: ParamsComponent },
    { path: "/form", name: "Form", component: FormComponent },
    { path: "/directive", name: "Directive", component: DirectiveComponent },
    { path: "/http", name: "Http", component: HttpComponent },
    { path: "/services", name: "Services", component: ServicesComponent },
    { path: "/dataflow", name: "Dataflow", component: DataFlowComponent },
    { path: "/pipes", name: "Pipes", component: PipesComponent }
])

export class AppComponent { }
