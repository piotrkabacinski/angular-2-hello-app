import { Component } from "angular2/core";
import { basePath, MenuComponent, HeaderComponent, SubpageComponent } from "../common/index";
import { BiggieDirective } from "./biggie.directive";

@Component({
    templateUrl: basePath + "/directive/directive.template.html",
    directives: [MenuComponent, HeaderComponent, SubpageComponent, BiggieDirective]
})

export class DirectiveComponent {

    private title: string = "Dyrektywa";

}
