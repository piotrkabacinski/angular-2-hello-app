import { Directive, ElementRef } from "angular2/core";

@Directive({
    selector: "[biggie]"
})

export class BiggieDirective {

    constructor(
        private element: ElementRef
    ) {

        function setCharAt(text, index, character) {
            return text.substr(0, index) + character + text.substr(index + character.length);
        }

        element.nativeElement.classList.add("biggie");

        let text = element.nativeElement.innerHTML,
            current = 0,
            letter = undefined;

        setInterval(function() {

            letter = text.charAt(current).toUpperCase();

            element.nativeElement.innerHTML = setCharAt(text, current, letter);

            current++;

            if (current > text.length) {
                current = 0;
            }

        }, 500);

    }
}
