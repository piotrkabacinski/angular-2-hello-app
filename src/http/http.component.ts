import { Component } from "angular2/core";
import { Http } from "angular2/http";
import { basePath, MenuComponent, HeaderComponent, SubpageComponent, StashService, UsersTableComponent } from "../common/index";

@Component({
    templateUrl: basePath + "/http/http.template.html",
    directives: [MenuComponent, HeaderComponent, SubpageComponent, UsersTableComponent],
    providers: [StashService]
})

export class HttpComponent {

    private users: Array<string> = [];
    private title: string = "REST";

    constructor(private http: Http) { }

    get() {

        this.http.get(basePath + "/http/mocks/example.json")
                 .map(response => response.json())
                 .subscribe(
                   data => {
                       this.users = data;
                   },
                   error => {
                       console.log(error);
                   }
                 );
    }

    stash() {

      StashService.create("users");
      StashService.add("users", this.users);

    }

}
