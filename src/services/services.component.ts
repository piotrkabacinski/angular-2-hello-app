import { Component } from "angular2/core";
import { basePath, MenuComponent, HeaderComponent, SubpageComponent, StashService, SortService, UsersTableComponent } from "../common/index";

@Component({
    templateUrl: basePath + "/services/services.template.html",
    directives: [MenuComponent, HeaderComponent, SubpageComponent, UsersTableComponent],
    providers: [StashService, SortService]
})

export class ServicesComponent {

    private title: string = "Serwisy";
    private users: Array<string> = [];
    private order: string = "asc";

    constructor(private sorter: SortService) {

        this.users = StashService.get("users") || [];

    }

    sort() {

        this.order = (this.order === "asc") ? "desc" : "asc";
        this.users = this.sorter.sort(this.users, "name", this.order);

    }

}
