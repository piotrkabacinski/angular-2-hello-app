'use strict';

var gulp = require('gulp'),
    ts = require('gulp-typescript'),
    watch = require('gulp-watch'),
    clean = require('gulp-clean'),
    tsOptions = require('./tsconfig.json'),
    sass = require('gulp-sass'),
    replace = require('gulp-replace');

    // This entry is only for non Gulp compilers
    delete tsOptions.compilerOptions.buildOnSave;

// Compile *.ts to *.js
gulp.task( 'compile', ['clear'] , function () {
  return gulp.src([ 'node_modules/angular2/typings/browser.d.ts' , 'src/**/*.ts' ])
    .pipe( ts(tsOptions.compilerOptions) )
    .pipe( gulp.dest('./dist') );
});

// Sass
gulp.task( 'sass', ['clear'], function () {
  return gulp.src( './src/css/style.scss' )
    .pipe(sass({
    	outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});

// Copy static dependencies to dist/vendor
gulp.task('vendors', function() {
   return gulp.src([
    'node_modules/es6-shim/es6-shim.min.js' ,
    'node_modules/systemjs/dist/system-polyfills.js',
    'node_modules/angular2/bundles/angular2-polyfills.js',
    'node_modules/systemjs/dist/system.src.js',
    'node_modules/rxjs/bundles/Rx.min.js',
    'node_modules/angular2/bundles/angular2.min.js',
    'node_modules/angular2/bundles/angular2.dev.js',
    'node_modules/angular2/bundles/angular2.js',
    'node_modules/angular2/es6/dev/src/testing/shims_for_IE.js',
    'node_modules/angular2/bundles/http.dev.js',
    'node_modules/angular2/bundles/router.dev.js'
  ])
  .pipe( gulp.dest('./dist/vendor') );
});

// Clear compiled JS files
gulp.task('clear', function() {
   return gulp.src( [ 'dist/**/*.*' , '!dist/vendor/*.js', '!dist/css/foundation.min.css' ], { read: false } )
   .pipe(clean());
});

// Copy compiled *.ts files to dist/app
gulp.task('copy-assets', ['compile'], function() {
   return gulp.src([ 'src/**/*.*' , '!src/**/*.ts', '!src/**/*.scss' ])
   .pipe( gulp.dest('./dist') );
});

// Compilation timestamp at the end of process
gulp.task('version', ['clear', 'compile', 'copy-assets'], function() {
    return gulp.src(['index.html'])
    .pipe(replace( /js\?v=(.*)'/g, 'js?v='+ new Date().getTime() + '\''))
    .pipe( gulp.dest('') );
});

// Watch
gulp.task('watch', function() {
  gulp.watch(
    ['src/**/*.*'] , [ 'default' ]
  );
});

gulp.task( 'default' , [ 'clear' , 'compile' , 'copy-assets' , 'version', 'sass' ] );
