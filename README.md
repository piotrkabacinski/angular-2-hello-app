# Angular 2.0.0-beta.15 Hello App

[Prezentacja](https://docs.google.com/presentation/d/1jdtX_Kz0kxZ3fjvWCUEvpce66-8KZJEjaSRwc5Dg9AE/edit?usp=sharing)

[Angular 2 Quickstart](https://github.com/angular/quickstart)

```bash
$ sudo npm i
$ gulp vendors # Copy static dependencies to dist/vendor
$ gulp # Compile *.ts files and propagte them to dist/
```

```bash
$ gulp watch # Watch and compile *.ts files
```

## Sources
[Angular 2 Docs](https://angular.io/docs/ts/latest/)
